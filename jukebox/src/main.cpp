#include <boost/log/core/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/keywords/format.hpp>
#include <boost/log/utility/setup/console.hpp>
#include <boost/program_options.hpp>
#include <pwd.h>
#include <raspberry_jukebox/common.h>
#include <raspberry_jukebox/config.h>
#include <raspberry_jukebox/handler.h>
#include <raspberry_jukebox/receiver.h>
#include <raspberry_jukebox/receiver_errors.h>
#include <rpi_jukebox_config.pb.h>
#include <rpi_jukebox_events.pb.h>
#include <unistd.h>
#include <experimental/filesystem>
#include <iostream>
#include <map>
#include <memory>
#include <string>

namespace po = boost::program_options;
namespace rj = raspberry_jukebox;

static const constexpr char* DEFAULT_CONFIG_FILE = "/.raspberry_jukebox/actions.conf";
static const constexpr char* DEFAULT_MUSIC_DIR = "/music/";

int main(int argc, char **argv) {

	struct passwd *pw = getpwuid(getuid());
	const std::string homedir(pw->pw_dir);

	po::options_description desc("Allowed options");
	desc.add_options()
	    ("help,h", "produce help message")
		("verbose,v", "Print debug messages to stdout")
		("quiet,q", "Print only errors")
	    ("uri", po::value<std::string>(), "set the uri of the event receiver")
		("config-file,c", po::value<std::string>()->default_value(homedir + DEFAULT_CONFIG_FILE), "set the path to config file")
		("working-directory, w", po::value<std::string>(), "set the path to music files")
		("store,s", "Store the uri, log-directory and working directory in config file");

	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);

	if (vm.count("help")) {
	    std::cout << desc << "\n";
	    return 0;
	}

	rj::config::ConfigContainer container{vm["config-file"].as<std::string>()};

	if(vm.count("uri") > 0) {
		container.m_config.set_zmq_uri(vm["uri"].as<std::string>());
	}

	const std::string zmq_uri = container.m_config.zmq_uri().empty() ? raspberry_jukebox::DEFAULT_URI_SERVER : container.m_config.zmq_uri();

	if(vm.count("working-directory") > 0) {
		container.m_config.set_working_directory(vm["working-directory"].as<std::string>());
	}
	std::experimental::filesystem::path working_directory =
			container.m_config.working_directory().empty() ?
					std::string(homedir + DEFAULT_MUSIC_DIR) :
					container.m_config.working_directory();
	container.m_config.set_working_directory(working_directory.string());
	container.sanitize_config_working_directory();

	auto severity = vm.count("verbose") > 0 ? boost::log::trivial::debug : boost::log::trivial::info;
	severity = vm.count("quiet") > 0 ? boost::log::trivial::error : severity;
	boost::log::aux::add_console_log(std::cout, boost::log::keywords::format = ">> %Message%");
	boost::log::core::get()->set_filter(boost::log::trivial::severity >= severity);

	BOOST_LOG_TRIVIAL(debug) << "Read config file from: " << vm["config-file"].as<std::string>();
	if(vm.count("store") > 0) {
		BOOST_LOG_TRIVIAL(debug) << "Store configuration: " << vm["config-file"].as<std::string>();
		container.update_config();
	}
	BOOST_LOG_TRIVIAL(info) << "Connect to: " << zmq_uri;
	BOOST_LOG_TRIVIAL(info) << "Directory for music: " << container.m_config.working_directory();

	rj::ZMQReceiver receiver(zmq_uri);

	std::map<rpi_jukebox_events::RPC_ID, std::function<std::unique_ptr<rpi_jukebox_events::RpcResult>(const rpi_jukebox_events::Rpc&, rj::config::ConfigContainer&)>> handlers;
	handlers[rpi_jukebox_events::RPC_ID::RPC_QUERY_REGISTERED] = rj::handle_query_registered;
	handlers[rpi_jukebox_events::RPC_ID::RPC_REGISTER] = rj::handle_register;
	handlers[rpi_jukebox_events::RPC_ID::RPC_QUERY_AVAILABLE] = rj::handle_query_available;
	handlers[rpi_jukebox_events::RPC_ID::RPC_QUERY_AVAILABLE_FILES] = rj::handle_query_available_files;
	handlers[rpi_jukebox_events::RPC_ID::RPC_DELETE] = rj::handle_delete;

	rj::ExecuteHandler ex_handler{container};

	while (true) {
		BOOST_LOG_TRIVIAL(debug) << "Wait for next RPC";
		try {
			auto rpc = receiver.get_next_rpc();
			BOOST_LOG_TRIVIAL(debug) << "Received RPC: " << rpc->ShortDebugString();
			std::unique_ptr<rpi_jukebox_events::RpcResult> result = nullptr;
			if(rpc->id() == rpi_jukebox_events::RPC_ID::RPC_EXECUTE) {
				result = ex_handler.handle_execute(*rpc, container);
			} else {
				result = handlers.at(rpc->id())(*rpc, container);
			}
			BOOST_LOG_TRIVIAL(debug) << "Send result: " << result->ShortDebugString();
			receiver.send_rpc_execution_result(*result);
		} catch(rj::receiver::MissingSendresultError& e) {
			BOOST_LOG_TRIVIAL(error) << "Previous RPC was not answered correctly - try to recover";
			rpi_jukebox_events::RpcResult result;
			result.set_result(rpi_jukebox_events::RESULT::RES_ERROR);
			receiver.send_rpc_execution_result(result);
		} catch(std::exception& e) {
			BOOST_LOG_TRIVIAL(error) << e.what();
			rpi_jukebox_events::RpcResult result;
			result.set_result(rpi_jukebox_events::RESULT::RES_ERROR);
			receiver.send_rpc_execution_result(result);
		}
	}

	return 0;
}
