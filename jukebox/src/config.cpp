#include <raspberry_jukebox/config.h>
#include "rpi_jukebox_config.pb.h"

#include <algorithm>
#include <experimental/filesystem>
#include <fstream>
#include <iostream>

namespace fs = std::experimental::filesystem;

void raspberry_jukebox::config::ConfigContainer::sanitize_config_working_directory() {
	// There shall be exactly one trailing separator
	auto temp_path = this->m_config.working_directory();
	while(temp_path.end() != temp_path.begin() &&
	      *(temp_path.end() - 1) == std::experimental::filesystem::path::preferred_separator) {
		temp_path = temp_path.substr(0, temp_path.size() - 1);
	};
	this->m_config.set_working_directory(temp_path + std::experimental::filesystem::path::preferred_separator);
}

raspberry_jukebox::config::ConfigContainer::ConfigContainer() : m_config(), m_path() {}

raspberry_jukebox::config::ConfigContainer::ConfigContainer(const std::experimental::filesystem::path& filename) : m_config(), m_path(filename) {
	auto input = new std::fstream(filename.string(), std::ios::binary | std::ios::in);
	this->m_config.ParseFromIstream(input);
	this->sanitize_config_working_directory();
}

void raspberry_jukebox::config::ConfigContainer::update_config() {
	try {
		std::experimental::filesystem::remove(this->m_path);
	} catch (const std::experimental::filesystem::filesystem_error&) {
	}

	std::experimental::filesystem::create_directories(this->m_path.parent_path());

	auto output = new std::fstream(this->m_path.string(), std::ios::binary | std::ios::trunc | std::ios::out);
	this->m_config.SerializeToOstream(output);
	output->flush();
}

static bool is_valid_extension(const fs::path& p) {
	std::vector<std::string> ext = { ".mp3", ".ogg" };
	return std::find(std::begin(ext), std::end(ext), p.extension()) != std::end(ext);
}

std::unique_ptr<std::vector<std::string>> raspberry_jukebox::config::get_available_files(const std::experimental::filesystem::path& working_directory) {
	auto result = std::make_unique<std::vector<std::string>>();
	try {
		for (const auto& p : fs::recursive_directory_iterator(working_directory)) {
			if(fs::is_regular_file(p.path()) && is_valid_extension(p.path())) {
				result->push_back(p.path().string().substr(working_directory.string().size()));
			}
		}
	}catch(fs::filesystem_error& ) {
	}
	std::sort(result->begin(), result->end());
	return result;
}

std::vector<std::string> raspberry_jukebox::config::get_available_subdirectories(const std::experimental::filesystem::path& working_directory) {
	std::vector<std::string> result;
	try {
		for (const auto& p : fs::recursive_directory_iterator(working_directory)) {
			if (fs::is_directory(p.path())) {
				for (const auto& test_p : fs::directory_iterator(p.path())) {
					if (fs::is_regular_file(test_p.path()) && is_valid_extension(test_p.path())) {
						result.push_back(p.path().string().substr(working_directory.string().size()));
						break;
					}
				}
			}
		}
	} catch (fs::filesystem_error&) {
	}
	std::sort(result.begin(), result.end());
	return result;
}

std::vector<std::string> raspberry_jukebox::config::get_available_subdirectories_and_files(const std::experimental::filesystem::path& working_directory) {
	std::vector<std::string> result = get_available_subdirectories(working_directory);
	auto files = get_available_files(working_directory);
	result.insert(result.cend(), std::make_move_iterator(files->begin()), std::make_move_iterator(files->end()));
	return result;
}
