#include "raspberry_jukebox/handler.h"
#include "raspberry_jukebox_internals/handler_utils.h"
#include "rpi_jukebox_events.pb.h"

#include <algorithm>
#include <experimental/filesystem>
#include <iterator>
#include <stdexcept>
#include <string>
#include <vector>

namespace raspberry_jukebox {

namespace fs = std::experimental::filesystem;

std::unique_ptr<rpi_jukebox_events::RpcResult> handle_query_registered(
		const rpi_jukebox_events::Rpc& rpc,
		raspberry_jukebox::config::ConfigContainer& config) {
	internals::check_rpc_id(rpi_jukebox_events::RPC_ID::RPC_QUERY_REGISTERED, rpc);
	auto result = std::make_unique<rpi_jukebox_events::RpcResult>();
	result->set_result(rpi_jukebox_events::RESULT::RES_OK);
	result->mutable_event_actions()->CopyFrom(config.m_config.event_actions());
	return result;
}



std::unique_ptr<rpi_jukebox_events::RpcResult> handle_query_available(
		const rpi_jukebox_events::Rpc& rpc,
		raspberry_jukebox::config::ConfigContainer& config) {
	internals::check_rpc_id(rpi_jukebox_events::RPC_ID::RPC_QUERY_AVAILABLE, rpc);
	auto result = std::make_unique<rpi_jukebox_events::RpcResult>();
	result->set_result(rpi_jukebox_events::RESULT::RES_OK);
	auto available_files = config::get_available_subdirectories(config.m_config.working_directory());
	for (const std::string& f : available_files) {
		result->add_available_files(f);
	}
	return result;
}

std::unique_ptr<rpi_jukebox_events::RpcResult> handle_query_available_files(const rpi_jukebox_events::Rpc& rpc, raspberry_jukebox::config::ConfigContainer& config) {
	internals::check_rpc_id(rpi_jukebox_events::RPC_ID::RPC_QUERY_AVAILABLE_FILES, rpc);
	auto result = std::make_unique<rpi_jukebox_events::RpcResult>();
	result->set_result(rpi_jukebox_events::RESULT::RES_OK);
	auto available_files = config::get_available_files(config.m_config.working_directory());
	for (const std::string& f : *available_files) {
		result->add_available_files(f);
	}
	return result;
}

std::unique_ptr<rpi_jukebox_events::RpcResult> handle_register(const rpi_jukebox_events::Rpc& rpc, raspberry_jukebox::config::ConfigContainer& config) {
	internals::check_rpc_id(rpi_jukebox_events::RPC_ID::RPC_REGISTER, rpc);
	auto result = std::make_unique<rpi_jukebox_events::RpcResult>();
	if(!rpc.has_event_action()) {
		throw std::invalid_argument("No event action for register RPC");
	}

	if(!rpc.event_action().has_event()) {
		throw std::invalid_argument("No event in event action for register RPC");
	}

	if(rpc.event_action().event().rfid_uid() == 0 && rpc.event_action().event().uid().empty()) {
		throw std::invalid_argument("Neither RFID UID or UID given");
	}

	if(rpc.event_action().action() == rpi_jukebox_events::ACTION::ACTION_INVALID) {
		throw std::invalid_argument("No action given");
	}

	result->set_result(rpi_jukebox_events::RESULT::RES_OK);

	if(rpc.event_action().action() == rpi_jukebox_events::ACTION::ACTION_PLAY) {
		auto files = config::get_available_subdirectories_and_files(config.m_config.working_directory());
		auto comp = [&rpc](const std::string& s) {
			return s == rpc.event_action().filename();
		};
		if (files.cend() == std::find_if(files.cbegin(), files.cend(), comp)) {
			throw std::invalid_argument("Filename : \"" + rpc.event_action().filename() + "\" not found");
		}
	}
	auto element = internals::find_mutable_event_action(config.m_config.mutable_event_actions(), rpc.event_action().event().uid(), rpc.event_action().event().rfid_uid());
	if(element == config.m_config.mutable_event_actions()->end()) {
		config.m_config.add_event_actions()->CopyFrom(rpc.event_action());
	} else if(rpc.event_action().action() == rpi_jukebox_events::ACTION::ACTION_PLAY) {
		element->set_filename(rpc.event_action().filename());
	}
	config.update_config();
	return result;
}

std::unique_ptr<rpi_jukebox_events::RpcResult> handle_delete(const rpi_jukebox_events::Rpc& rpc, raspberry_jukebox::config::ConfigContainer& config) {
	internals::check_rpc_id(rpi_jukebox_events::RPC_ID::RPC_DELETE, rpc);
	auto result = std::make_unique<rpi_jukebox_events::RpcResult>();
	result->set_result(rpi_jukebox_events::RESULT::RES_OK);
	if(rpc.has_event_action() && rpc.event_action().has_event()) {
		auto element = internals::find_event_action(config.m_config.event_actions(), rpc.event_action().event().uid(), rpc.event_action().event().rfid_uid());
		if(element != std::end(config.m_config.event_actions())) {
			config.m_config.mutable_event_actions()->erase(element);
		}
		config.update_config();
	}
	return result;
}

}

