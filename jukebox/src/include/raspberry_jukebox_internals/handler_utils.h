#pragma once

#include "rpi_jukebox_events.pb.h"

namespace raspberry_jukebox {
namespace internals {
	void check_rpc_id(rpi_jukebox_events::RPC_ID id,
		const rpi_jukebox_events::Rpc& rpc);

	decltype(std::declval<rpi_jukebox_events::RpcResult>().event_actions().cbegin()) find_event_action(
		decltype(std::declval<rpi_jukebox_events::RpcResult>().event_actions()) event_actions,
		const std::string& uid, uint64_t rfid_uid);

	decltype(std::declval<rpi_jukebox_events::RpcResult>().mutable_event_actions()->begin()) find_mutable_event_action(
		decltype(std::declval<rpi_jukebox_events::RpcResult>().mutable_event_actions()) event_actions,
		const std::string& uid, uint64_t rfid_uid);
}
}
