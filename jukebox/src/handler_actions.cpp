#include <raspberry_jukebox/config.h>
#include <raspberry_jukebox/handler.h>

#include <boost/log/trivial.hpp>

#include <algorithm>
#include <memory>
#include <iostream>

bool operator==(const rpi_jukebox_events::Event& lh, const rpi_jukebox_events::Event& rh) {
	return (lh.uid() == rh.uid()) && (lh.rfid_uid() == rh.rfid_uid());
}

std::unique_ptr<rpi_jukebox_events::RpcResult> raspberry_jukebox::ExecuteHandler::handle_action_play(const rpi_jukebox_events::Rpc& rpc, raspberry_jukebox::config::ConfigContainer& config) {
	auto result = std::make_unique<rpi_jukebox_events::RpcResult>();
	if(!rpc.has_event() || (rpc.event().rfid_uid() == 0 && rpc.event().uid().empty())) {
		result->set_result(rpi_jukebox_events::RESULT::RES_ERROR);
		return result;
	}

	result->set_result(rpi_jukebox_events::RESULT::RES_OK);
	auto cmp = [&rpc](const rpi_jukebox_events::EventAction& ea) {
		return rpc.event() == ea.event();
	};
	auto known_id = std::find_if(std::cbegin(config.m_config.event_actions()), std::cend(config.m_config.event_actions()), cmp);
	if(known_id == std::cend(config.m_config.event_actions())) {
		BOOST_LOG_TRIVIAL(warning) << "No PLAY ACTION registered for event: " << rpc.event().ShortDebugString();
		result->set_result(rpi_jukebox_events::RESULT::RES_ERROR);
		return result;
	}
	if(this->m_vlc_media.find(known_id->filename()) == this->m_vlc_media.end()) {
		if(this->m_vlc_media_list.find(known_id->filename()) == this->m_vlc_media_list.end()) {
			BOOST_LOG_TRIVIAL(warning) << "Can't find filename in vlc medias: " << known_id->filename();
			result->set_result(rpi_jukebox_events::RESULT::RES_ERROR);
			return result;
		}
	}
	this->stop_and_release_media_player();
	if(this->m_vlc_media.find(known_id->filename()) != this->m_vlc_media.end()) {
		BOOST_LOG_TRIVIAL(debug) << "Create media player for " << known_id->filename();
		this->m_vlc_media_player = libvlc_media_player_new_from_media(this->m_vlc_media.at(known_id->filename()));
	} else {
		BOOST_LOG_TRIVIAL(debug) << "Create media list player for " << known_id->filename();
		this->m_vlc_media_list_player = libvlc_media_list_player_new(this->m_vlc_inst);
		libvlc_media_list_player_set_media_list(this->m_vlc_media_list_player, this->m_vlc_media_list.at(known_id->filename()));
	}
	auto rc = this->play_media_player();
	if(rc) {
		BOOST_LOG_TRIVIAL(error) << "Can't play media: " << rc;
		result->set_result(rpi_jukebox_events::RESULT::RES_ERROR);
	} else {
		result->set_result(rpi_jukebox_events::RESULT::RES_OK);
	}
	return result;
}
std::unique_ptr<rpi_jukebox_events::RpcResult> raspberry_jukebox::ExecuteHandler::handle_action_pause() {
	BOOST_LOG_TRIVIAL(debug) << "Calling action pause";
	auto result = std::make_unique<rpi_jukebox_events::RpcResult>();
	result->set_result(rpi_jukebox_events::RESULT::RES_OK);
	this->pause_media_player();
	return result;
}
std::unique_ptr<rpi_jukebox_events::RpcResult> raspberry_jukebox::ExecuteHandler::handle_action_stop() {
	BOOST_LOG_TRIVIAL(debug) << "Calling action stop";
	auto result = std::make_unique<rpi_jukebox_events::RpcResult>();
	result->set_result(rpi_jukebox_events::RESULT::RES_OK);
	this->stop_and_release_media_player();
	return result;
}
std::unique_ptr<rpi_jukebox_events::RpcResult> raspberry_jukebox::ExecuteHandler::handle_action_forward() {
	BOOST_LOG_TRIVIAL(debug) << "Calling action forward";
	auto result = std::make_unique<rpi_jukebox_events::RpcResult>();
	result->set_result(rpi_jukebox_events::RESULT::RES_OK);
	this->forward_media_player();
	return result;
}
std::unique_ptr<rpi_jukebox_events::RpcResult> raspberry_jukebox::ExecuteHandler::handle_action_rewind() {
	BOOST_LOG_TRIVIAL(debug) << "Calling action rewind";
	auto result = std::make_unique<rpi_jukebox_events::RpcResult>();
	result->set_result(rpi_jukebox_events::RESULT::RES_OK);
	this->rewind_media_player();
	return result;
}
