#include "raspberry_jukebox/provider.h"
// cppcheck-suppress missingInclude
#include "boost/program_options.hpp"
#include <iostream>
#include <memory>

namespace po = boost::program_options;

int main(int argc, char **argv) {

	po::options_description desc("Allowed options");
	desc.add_options()
	    ("help", "produce help message")
	    ("uri", po::value<std::string>(), "set the uri of the event getter");

	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);

	if (vm.count("help")) {
	    std::cout << desc << "\n";
	    return 0;
	}

	std::unique_ptr<raspberry_jukebox::RpcSender> provider = nullptr;
	std::unique_ptr<raspberry_jukebox::EventReader> reader = nullptr;

	if (vm.count("uri")) {
		provider = std::make_unique<raspberry_jukebox::RpcSender>(std::move(reader), vm["uri"].as<std::string>());
	}



	return 0;
}

