#include "raspberry_jukebox/common.h"
#include "raspberry_jukebox/provider.h"
#include "raspberry_jukebox/provider_errors.h"

#include <iostream>

raspberry_jukebox::RpcSender::RpcSender(const std::string& uri) :
		m_zmq_context(), m_zmq_socket(m_zmq_context, ZMQ_REQ), m_uri(uri) {

	this->m_zmq_socket.connect(m_uri.c_str());
}

std::unique_ptr<rpi_jukebox_events::RpcResult> raspberry_jukebox::RpcSender::fetch_rpc_result() {
	zmq::message_t zmq_msg;
	this->m_zmq_socket.recv(&zmq_msg);
	std::string resp_msg_str(static_cast<char*>(zmq_msg.data()), zmq_msg.size());
	auto result = std::make_unique<rpi_jukebox_events::RpcResult>();
	if (!result->ParseFromString(resp_msg_str)) {
		throw MessageMalformedError();
	}
	return result;
}

void raspberry_jukebox::RpcSender::send_rpc(const rpi_jukebox_events::Rpc& rpc) {
	std::string msg_str;
	rpc.SerializeToString(&msg_str);
	zmq::message_t request(msg_str.size());
	std::memcpy((void *) request.data(), msg_str.c_str(), msg_str.size());
	try {
		this->m_zmq_socket.send(request);
	} catch (zmq::error_t& err) {
		if (err.num() == EFSM)
			throw provider::MissingGetLastResultError();
		else
			throw err;  // all other errors are not handled
	}
}

std::unique_ptr<rpi_jukebox_events::RpcResult> raspberry_jukebox::RpcSender::rpc(rpi_jukebox_events::RPC_ID rpc_id) {
	rpi_jukebox_events::Rpc rpc;
	rpc.set_id(rpc_id);
	this->send_rpc(rpc);
	return this->fetch_rpc_result();
}

std::unique_ptr<rpi_jukebox_events::RpcResult> raspberry_jukebox::RpcSender::rpc(rpi_jukebox_events::RPC_ID rpc_id, std::unique_ptr<rpi_jukebox_events::EventAction> event_action) {
	rpi_jukebox_events::Rpc rpc;
	rpc.set_id(rpc_id);
	rpc.set_allocated_event_action(&(*event_action));
	event_action.release();
	this->send_rpc(rpc);
	return this->fetch_rpc_result();
}

std::unique_ptr<rpi_jukebox_events::RpcResult> raspberry_jukebox::RpcSender::rpc(rpi_jukebox_events::RPC_ID rpc_id, std::unique_ptr<rpi_jukebox_events::Event> event) {
	rpi_jukebox_events::Rpc rpc;
	rpc.set_id(rpc_id);
	rpc.set_allocated_event(&(*event));
	event.release();
	this->send_rpc(rpc);
	return this->fetch_rpc_result();
}

std::string raspberry_jukebox::RpcSender::to_str() const {
	return this->m_uri;
}

std::ostream& operator<<(std::ostream& ostream, const raspberry_jukebox::RpcSender& provider) {
	ostream << provider.to_str();
	return ostream;
}
