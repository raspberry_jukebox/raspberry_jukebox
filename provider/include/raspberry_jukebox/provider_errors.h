#pragma once

#include "raspberry_jukebox/common_errors.h"

#include <exception>

namespace raspberry_jukebox {
namespace provider {

class MissingGetLastResultError: public std::exception {
};

class MissingPushEventError: public std::exception {
};

}
}
