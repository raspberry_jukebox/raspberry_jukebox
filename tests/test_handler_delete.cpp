#include "raspberry_jukebox/handler.h"
#include "rpi_jukebox_config.pb.h"
#include "rpi_jukebox_events.pb.h"

#include <gtest/gtest.h>
#include <experimental/filesystem>

using namespace rpi_jukebox_events;
using namespace rpi_jukebox_config;
using namespace raspberry_jukebox;
using namespace raspberry_jukebox::config;

TEST(Handler, WrongRPC) {
	std::experimental::filesystem::path temp_file = "test_files_temp_dir/test_file";
	ConfigContainer config{temp_file};
	Rpc rpc{};
	EXPECT_THROW(handle_register(rpc, config), std::exception);
	rpc.set_id(RPC_ID::RPC_EXECUTE);
	EXPECT_THROW(handle_register(rpc, config), std::exception);
	EXPECT_FALSE(std::experimental::filesystem::exists(temp_file));
}

TEST(DeleteActions, EmptyConfig) {
	std::experimental::filesystem::path temp_file = "test_files_temp_dir/test_file";
	ConfigContainer config{temp_file};
	Rpc rpc{};
	rpc.set_id(RPC_ID::RPC_DELETE);
	auto result = handle_delete(rpc, config);
	EXPECT_EQ(RESULT::RES_OK, result->result());
	EXPECT_FALSE(std::experimental::filesystem::exists(temp_file));
	try {
		std::experimental::filesystem::remove(temp_file);
	}catch(std::exception&) {}
}

TEST(DeleteActions, SingleEventAction) {
	std::experimental::filesystem::path temp_file = "test_files_temp_dir/test_file";
	ConfigContainer config{temp_file};

	EventAction ea{};
	ea.set_action(ACTION::ACTION_PLAY);
	ea.mutable_event()->set_rfid_uid(0x1234);
	ea.mutable_event()->set_uid("TestUID");
	EXPECT_NE(ea.event().uid(), std::string());
	EXPECT_NE(ea.event().rfid_uid(), 0);
	ea.set_filename("test_file");
	ea.set_working_directory("tests");
	config.m_config.add_event_actions()->CopyFrom(ea);

	Rpc rpc{};
	rpc.set_id(RPC_ID::RPC_DELETE);
	rpc.mutable_event_action()->CopyFrom(ea);

	auto result = handle_delete(rpc, config);

	EXPECT_EQ(result->result(), RESULT::RES_OK);
	EXPECT_EQ(0, config.m_config.event_actions().size());
	EXPECT_TRUE(std::experimental::filesystem::exists(temp_file));
	std::experimental::filesystem::remove(temp_file);
}
