// cppcheck-suppress missingInclude
#include "rpi_jukebox_events.pb.h"
#include "raspberry_jukebox/receiver.h"
#include "raspberry_jukebox/receiver_errors.h"

#include <gtest/gtest.h>

using namespace raspberry_jukebox;

TEST(Simple, StartReceiver) {
	ZMQReceiver receiver(DEFAULT_URI_SERVER);
	EXPECT_EQ(receiver.to_str(), std::string(DEFAULT_URI_SERVER));
}

TEST(Simple, StartReceiverWithConnection) {
	ZMQReceiver receiver(DEFAULT_URI_SERVER);

	zmq::context_t context;
	zmq::socket_t sock(context, ZMQ_REQ);
	sock.connect(DEFAULT_URI_CLIENT);

	{
		rpi_jukebox_events::Rpc rpc;
		rpc.set_id(rpi_jukebox_events::RPC_EXECUTE);
		rpi_jukebox_events::Event* event = rpc.mutable_event();
		event->set_uid("Hello");
		std::string msg_str;
		ASSERT_TRUE(rpc.SerializeToString(&msg_str));
		zmq::message_t request(msg_str.size());
		std::memcpy((void *) request.data(), msg_str.c_str(), msg_str.size());
		sock.send(request);

		auto rec_event = receiver.get_next_rpc();
		EXPECT_TRUE(rec_event->has_event());
		EXPECT_FALSE(rec_event->has_event_action());
		EXPECT_EQ(std::string("Hello"), rec_event->event().uid());
		rpi_jukebox_events::RpcResult result;
		result.set_result(rpi_jukebox_events::RES_OK);
		receiver.send_rpc_execution_result(result);

		zmq::message_t zmq_result_msg;
		ASSERT_TRUE(sock.recv(&zmq_result_msg));
		std::string res_msg_str(static_cast<char*>(zmq_result_msg.data()), zmq_result_msg.size());
		EXPECT_TRUE(result.ParseFromString(res_msg_str));
		EXPECT_EQ(result.result(), rpi_jukebox_events::RES_OK);
	}
	{
		rpi_jukebox_events::Rpc rpc;
		rpc.set_id(rpi_jukebox_events::RPC_EXECUTE);
		rpi_jukebox_events::Event* event = rpc.mutable_event();
		event->set_uid("Hello World");
		std::string msg_str;
		ASSERT_TRUE(rpc.SerializeToString(&msg_str));
		zmq::message_t request(msg_str.size());
		std::memcpy((void *) request.data(), msg_str.c_str(), msg_str.size());
		sock.send(request);

		auto rec_event = receiver.get_next_rpc();
		EXPECT_EQ(std::string("Hello World"), rec_event->event().uid());
		rpi_jukebox_events::RpcResult result;
		result.set_result(rpi_jukebox_events::RES_OK);
		receiver.send_rpc_execution_result(result);

		zmq::message_t zmq_result_msg;
		ASSERT_TRUE(sock.recv(&zmq_result_msg));
		std::string res_msg_str(static_cast<char*>(zmq_result_msg.data()), zmq_result_msg.size());
		EXPECT_TRUE(result.ParseFromString(res_msg_str));
		EXPECT_EQ(result.result(), rpi_jukebox_events::RES_OK);
	}
}

TEST(Simple, SocketWrongState) {
	ZMQReceiver receiver(DEFAULT_URI_SERVER);

	zmq::context_t context;
	zmq::socket_t sock(context, ZMQ_REQ);
	sock.connect(DEFAULT_URI_CLIENT);

	rpi_jukebox_events::Event event;
	event.set_uid("Hello");
	std::string msg_str;
	ASSERT_TRUE(event.SerializeToString(&msg_str));
	zmq::message_t request(msg_str.size());
	std::memcpy((void *) request.data(), msg_str.c_str(), msg_str.size());
	sock.send(request);

	receiver.get_next_rpc();
	EXPECT_THROW(receiver.get_next_rpc(), receiver::MissingSendresultError);
}

TEST(Simple, SocketWrongStateSendResult) {
	ZMQReceiver receiver(DEFAULT_URI_SERVER);
	EXPECT_THROW(receiver.send_rpc_execution_result(rpi_jukebox_events::RpcResult()),
			receiver::MissingGetNextEventError);
}

TEST(Simple, MessageMalformed) {
	ZMQReceiver receiver(DEFAULT_URI_SERVER);

	zmq::context_t context;
	zmq::socket_t sock(context, ZMQ_REQ);
	sock.connect(DEFAULT_URI_CLIENT);

	std::string msg_str("Hello");
	zmq::message_t request(msg_str.size());
	std::memcpy((void *) request.data(), msg_str.c_str(), msg_str.size());
	sock.send(request);

	EXPECT_THROW(receiver.get_next_rpc(), MessageMalformedError);

	rpi_jukebox_events::RpcResult result;
	result.set_result(rpi_jukebox_events::RES_ERROR);
	receiver.send_rpc_execution_result(result);

	zmq::message_t zmq_result_msg;
	ASSERT_TRUE(sock.recv(&zmq_result_msg));
	std::string res_msg_str(static_cast<char*>(zmq_result_msg.data()), zmq_result_msg.size());
	EXPECT_TRUE(result.ParseFromString(res_msg_str));
	EXPECT_EQ(result.result(), rpi_jukebox_events::RES_ERROR);
}
