Overview
========
This project enables the raspberry pi as a small music player. It's main purpose is for the use by (small) children. Once there is a graphical user interface attached, it will not be used for any input but just displays whatever was defined.  
Music and Playlists are selected by putting RFID chips near the reader. In a simple card form, this can be easy handled by children. Playlists and RFID chips are registered by a special card or maybe by some other device over ethernet. I haven't decided yet ....

Install
=======
Requirements
------------

The following packages have to be installed to compile this project:
* zeromq (https://github.com/MonsieurV/ZeroMQ-RPi)
* cppzmq (https://github.com/zeromq/cppzmq)
* probobuf (compiler and lib)
* cmake
* boost-program-options
* boost-log
* libvlc-dev

Instructions
------------

* git clone https://gitlab.com/realbigflo/raspberry_jukebox.git
* mkdir build
* cd build
* cmake ../raspberry_jukebox
* make
* ctest

The cmake step automatically downloads and compiles googletest & googlemock.